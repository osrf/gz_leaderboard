# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/gz_leaderboard

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-others-gh-pages/#!/osrf/gz_leaderboard

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/gz_leaderboard

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

